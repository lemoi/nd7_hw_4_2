'use strict';

angular
    .module('myApp')
    .controller('MyAccountCtrl', function(MyAccountService) {

        var vm = this;

        vm.newAccount = MyAccountService.getAccount();
        vm.stored = !!vm.newAccount.name;

        vm.addAccount = function(myAccount) {
            console.log(myAccount);
            MyAccountService.saveAccount(vm.newAccount.name, vm.newAccount.email, vm.newAccount.phone);
            vm.stored = true;
            vm.accountForm.$setPristine();
        };

    })
;
