'use strict';

angular
    .module('myApp')
    .factory('MyAccountService', function() {

        let account = {};

        return {
            getAccount()  {
                return account;
            },

            saveAccount(name, email, phone) {
                account = {
                    'name': name,
                    'email': email,
                    'phone': phone
                };
            }
        };

    })
