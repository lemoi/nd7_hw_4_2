//jshint strict: false
exports.config = {

    allScriptsTimeout: 11000,

    specs: [
      //'*.js'
      '*spec.js'
    ],

    capabilities: {
      'browserName': 'chrome'
    },

    baseUrl: 'http://localhost:8000/',

    /*framework: 'jasmine',

    jasmineNodeOpts: {
      defaultTimeoutInterval: 30000
    }
    */

    framework: 'mocha',
    mochaOpts: {
        reporter: "spec",
        slow: 10000,
        timeout: 10000,
    },
    onPrepare: () => {
        const chai = require('chai');
        chai.use(require('chai-as-promised'));
    }

};
