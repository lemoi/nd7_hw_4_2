class AngularAccountPage {

    get url() {
        return 'http://localhost:8000/#!/myaccount';
    }

    get fieldName() {
        return element(by.model('vm.newAccount.name'));
    }

    get fieldEmail() {
        return element(by.model('vm.newAccount.email'));
    }

    get buttonAdd() {
        return element(by.css('button[data-ng-click="vm.addAccount(vm.newAccount)"]'));
    }

    get accountForm() {
        return browser.findElement(by.name('vm.accountForm'));
    }

    open() {
        browser.get(this.url);
    }

    setName(name) {
        this.fieldName.sendKeys(name);
    }

    setEmail(email) {
        this.fieldEmail.sendKeys(email);
    }

    formExist() {
        return this.accountForm.getTagName()
            .then(function(tagName) {
                return tagName === 'form';
            });
    }

    buttonAddEnabled() {
        return this.buttonAdd.isEnabled();
    }

}

module.exports = AngularAccountPage;