const expect = require('chai').expect;
var AngularAccountPage = require('./page-objects/angular-account-page');

describe('Задание 5.2. Тестируем клиент (Protractor) к заданию 4.2.', function() {

    it('Протестировать подсветку текущего пункта меню.', function() {

        browser.get('http://localhost:8000');

        var itemsMenu = element.all(by.repeater('menuItem in menuItems'));
        var itemClassActiveBtn = by.className('btn-primary');

        expect(browser.isElementPresent(itemClassActiveBtn)).eventually.to.be.false;

        itemsMenu.get(0).click();

        browser.sleep(500);

        expect(browser.isElementPresent(itemClassActiveBtn)).eventually.to.be.true;

        expect(element(itemClassActiveBtn).getText()).eventually.to.be.equal('Список');

    });

    it('Протестировать адрес /myaccount, убедиться что по нему открывается форма.', function() {

        var angularAccountPage = new AngularAccountPage();
        angularAccountPage.open();

        browser.sleep(1000);

        expect(angularAccountPage.formExist()).eventually.to.true;

        /*
        browser.get('http://localhost:8000/#!/myaccount');

        var accountForm = browser.findElement(by.name('vm.accountForm'));

        browser.sleep(1000);

        expect(accountForm.getTagName()).eventually.to.equal('form');
        */

    });

    it('Протестировать обязательные поля в форме.', function() {

        var angularAccountPage = new AngularAccountPage();
        angularAccountPage.open();

        angularAccountPage.setName('t');
        angularAccountPage.setEmail('email');

        browser.sleep(500);

        expect(angularAccountPage.buttonAddEnabled()).eventually.to.be.false;

        angularAccountPage.setName('est');
        angularAccountPage.setEmail('@gmail.com');

        browser.sleep(1000);

        expect(angularAccountPage.buttonAddEnabled()).eventually.to.be.true;

        /*
        browser.get('http://localhost:8000/#!/myaccount');

        var fieldName = element(by.model('vm.newAccount.name'));
        var fieldEmail = element(by.model('vm.newAccount.email'));
        var buttonAdd = element(by.css('button[data-ng-click="vm.addAccount(vm.newAccount)"]'));

        fieldName.sendKeys('t');
        fieldEmail.sendKeys('email');

        browser.sleep(500);

        expect(buttonAdd.isEnabled()).eventually.to.be.false;

        fieldName.sendKeys('est');
        fieldEmail.sendKeys('@gmail.com');

        browser.sleep(1000);

        expect(buttonAdd.isEnabled()).eventually.to.be.true;
        */

    });

    it('Протестировать добавление покемона в корзину.', function() {

        browser.get('http://localhost:8000/#!/list');

        var pokemons = element.all(by.repeater('singlePokemon in vm.pokemons'));
        var pokemonsShop = element.all(by.repeater('(singlePokemonIndex, singlePokemonValue) in $ctrl.cartItems'));

        pokemons.get(0).$('button[ng-click="vm.addToCart(singlePokemon)"]').click();

        browser.sleep(1000);

        expect(pokemonsShop.count()).eventually.to.equal(1);

    });

    it('Протестировать список покемонов. Убедиться что показано столько покемонов, сколько требуется.', function() {

        browser.get('http://localhost:8000/#!/list');

        var myQuery = element(by.model('vm.myQuery'));
        var pokemons = element.all(by.repeater('singlePokemon in vm.pokemons'));

        myQuery.sendKeys('SAUR');

        browser.sleep(1000);

        expect(pokemons.count()).eventually.to.equal(4);

    });

});
